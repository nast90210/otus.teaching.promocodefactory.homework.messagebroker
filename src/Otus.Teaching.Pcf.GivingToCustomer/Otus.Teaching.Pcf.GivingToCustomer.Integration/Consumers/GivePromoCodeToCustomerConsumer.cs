using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly ILogger<GivePromoCodeToCustomerConsumer> _logger;

        public GivePromoCodeToCustomerConsumer(ILogger<GivePromoCodeToCustomerConsumer> logger)
        {
            _logger = logger;
        }
        public Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            _logger.LogDebug($"Получил PromoCode с ID {context.Message.PromoCodeId}");
            return Task.CompletedTask;
        }
    }
}
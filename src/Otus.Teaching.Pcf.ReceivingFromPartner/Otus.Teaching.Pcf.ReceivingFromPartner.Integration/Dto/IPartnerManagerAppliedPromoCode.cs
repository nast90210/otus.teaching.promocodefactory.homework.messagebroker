using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public interface IPartnerManagerAppliedPromoCode
    {
        Guid PartnerManagerId { get; set; }
    }
}
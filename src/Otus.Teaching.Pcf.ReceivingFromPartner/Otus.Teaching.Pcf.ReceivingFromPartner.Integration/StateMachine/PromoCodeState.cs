using System;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.StateMachine
{
    public class PromoCodeState : SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
        public State CurrentState { get; set; }
        public PromoCode PromoCode { get; set; }
    }
}
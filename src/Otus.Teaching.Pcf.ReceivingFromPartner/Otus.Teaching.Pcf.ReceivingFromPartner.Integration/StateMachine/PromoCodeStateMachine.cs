using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.StateMachine
{
    public class PromoCodeStateMachine : MassTransitStateMachine<PromoCodeState>
    {
        public PromoCodeStateMachine()
        {
            InstanceState(state => state.CurrentState);

            Event(() => OnPromoCodeGet, context => context.CorrelateById(x => x.Message.Id));

            Initially(When(OnPromoCodeGet)
                .Then(context =>
                {
                    context.Saga.CorrelationId = context.Message.Id;
                    context.Saga.PromoCode = context.Message;
                })
                .PublishAsync(context => context.Init<GivePromoCodeToCustomerDto>(new GivePromoCodeToCustomerDto
                {
                    PromoCodeId = context.Saga.CorrelationId,
                    PartnerId = context.Saga.PromoCode.Partner.Id,
                    BeginDate = context.Saga.PromoCode.BeginDate.ToShortDateString(),
                    EndDate = context.Saga.PromoCode.EndDate.ToShortDateString(),
                    PreferenceId = context.Saga.PromoCode.PreferenceId,
                    PromoCode = context.Saga.PromoCode.Code,
                    ServiceInfo = context.Saga.PromoCode.ServiceInfo,
                    PartnerManagerId = context.Saga.PromoCode.PartnerManagerId
                }))
                .PublishAsync(context => context.Init<IPartnerManagerAppliedPromoCode>(new
                {
                    PartnerManagerId = context.Saga.PromoCode.PartnerManagerId
                }))
                .Finalize());
        }

        public Event<PromoCode> OnPromoCodeGet { get; set; }
        // public Event<GivePromoCodeToCustomerDto> OnGivePromoCodeToCustomer { get; set; }

        public State PromoCodeSendToCustomer { get; set; }
    }
}
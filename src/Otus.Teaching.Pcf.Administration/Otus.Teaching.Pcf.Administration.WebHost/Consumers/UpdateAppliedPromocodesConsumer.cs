using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class UpdateAppliedPromocodesConsumer : IConsumer<IPartnerManagerAppliedPromoCode>
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly ILogger<UpdateAppliedPromocodesConsumer> _logger;

        public UpdateAppliedPromocodesConsumer(
            IRepository<Employee> employeeRepository,
            ILogger<UpdateAppliedPromocodesConsumer> logger)
        {
            _employeeRepository = employeeRepository;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<IPartnerManagerAppliedPromoCode> context)
        {
            var employee = await _employeeRepository.GetByIdAsync(context.Message.PartnerManagerId);

            if (employee == null)
                _logger.LogWarning($"Партнер с ID {context.Message.PartnerManagerId} не найден");

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
            _logger.LogDebug($"Количество выданных PromoCodes у партнер с ID {context.Message.PartnerManagerId} обновлено");
        }
    }
}